using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace MetaConference
{
    public class AvatarRigEditor : Editor
    {
        private static void CreateHead(Transform rigParent, string nameHead)
        {
            GameObject ikHead = new GameObject(nameHead);
            ikHead.transform.parent = rigParent.transform;
            ikHead.AddComponent<MultiParentConstraint>();
        }

        private static void CreateIKHand(Transform rigParent,
                                         string nameHand,
                                         string nameTarget,
                                         string nameHint)
        {
            GameObject leftIkHand = new GameObject(nameHand);
            leftIkHand.transform.parent = rigParent.transform;
            leftIkHand.AddComponent<TwoBoneIKConstraint>();

            GameObject leftTarget = new GameObject(nameTarget);
            leftTarget.transform.parent = leftIkHand.transform;

            GameObject leftHint = new GameObject(nameHint);
            leftHint.transform.parent = leftIkHand.transform;
        }

        [MenuItem("Animation Rigging/Avatars/Complete Setup IK", false, priority = 0)]
        public static void CompleteAvatarRig()
        {
            CreateAvatarRig();
            SetupAvatarRig();
        }

        [MenuItem("Animation Rigging/Avatars/Parts/Create IK", false)]
        public static void CreateAvatarRig()
        {
            Transform selection = Selection.activeTransform;
            if (selection.TryGetComponent(out RigBuilder rigBuilder))
            {
                Transform rigParent = rigBuilder.layers[0].rig.transform;
                CreateIKHand(rigParent, "LeftIKArm", "Target", "Hint");
                CreateIKHand(rigParent, "RightIKArm", "Target", "Hint");
                CreateHead(rigParent, "IKHead");
            }
        }


        [MenuItem("Animation Rigging/Avatars/Parts/Setup IK", false)]
        public static void SetupAvatarRig()
        {
            Transform selection = Selection.activeTransform;
            if (selection.TryGetComponent(out RigBuilder rigBuilder))
            {
                Transform rigParent = rigBuilder.layers[0].rig.transform;

                Transform armature = selection.Find("Armature");
                if (armature == null)
                {
                    return;
                }

                string bodyPath = "Hips/Spine/Spine1/Spine2";
                string headPath = $"{bodyPath}/Neck/Head";

                string leftShoulderPath = $"{bodyPath}/LeftShoulder";
                string leftArmPath = $"{leftShoulderPath}/LeftArm";
                string leftForeArmPath = $"{leftArmPath}/LeftForeArm";
                string leftHandPath = $"{leftForeArmPath}/LeftHand";

                string rightShoulderPath = $"{bodyPath}/RightShoulder";
                string rightArmPath = $"{rightShoulderPath}/RightArm";
                string rightForeArmPath = $"{rightArmPath}/RightForeArm";
                string rightHandPath = $"{rightForeArmPath}/RightHand";

                TwoBoneIKConstraint[] twoBoneIKConstraints = rigParent.GetComponentsInChildren<TwoBoneIKConstraint>();
                MultiParentConstraint headIKConstraint = rigParent.GetComponentInChildren<MultiParentConstraint>();

                if (twoBoneIKConstraints.Length >= 2)
                {
                    SetupHand(twoBoneIKConstraints[0], armature, leftArmPath, leftForeArmPath, leftHandPath);
                    SetupHand(twoBoneIKConstraints[1], armature, rightArmPath, rightForeArmPath, rightHandPath);
                }

                if (headIKConstraint != null)
                {
                    SetupHead(headIKConstraint, armature, headPath);
                }
            }
        }

        private static void SetupHead(MultiParentConstraint ikConstraint, Transform armature, string headPath)
        {
            WeightedTransform weightedTransform = new WeightedTransform(ikConstraint.transform, 1f);
            WeightedTransformArray weightedTransformArray = new WeightedTransformArray();
            weightedTransformArray.Add(weightedTransform);
            ikConstraint.data.sourceObjects = weightedTransformArray;

            Transform headIK = armature.Find(headPath);
            ikConstraint.data.constrainedObject = headIK;
            ikConstraint.transform.SetPositionAndRotation(headIK.position, headIK.rotation);
        }

        private static void SetupHand(TwoBoneIKConstraint twoBoneIKConstraint,
                                      Transform armature,
                                      string armPath,
                                      string foreArmPath,
                                      string handPath)
        {
            TwoBoneIKConstraint handIKConstraint = twoBoneIKConstraint;
            Transform handRoot = armature.Find(armPath).transform;
            Transform handMid = armature.Find(foreArmPath).transform;
            Transform handTip = armature.Find(handPath).transform;
            handIKConstraint.data.root = handRoot;
            handIKConstraint.data.mid = handMid;
            handIKConstraint.data.tip = handTip;

            Transform target = handIKConstraint.transform.Find("Target");
            target.SetPositionAndRotation(handTip.position, handTip.rotation);
            Transform hint = handIKConstraint.transform.Find("Hint");
            hint.SetPositionAndRotation(handMid.position, handMid.rotation);

            handIKConstraint.data.target = target;
            handIKConstraint.data.hint = hint;
        }
    }
}