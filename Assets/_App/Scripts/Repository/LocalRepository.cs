﻿using System;
using UnityEngine;

namespace MetaConference.Repository
{
    public class LocalRepository: MonoBehaviour
    {
        public const string KEY_USER = "KEY_USER";

        public void SaveUser(UserData userData)
        {
            string jsonUser = JsonUtility.ToJson(userData);
            PlayerPrefs.SetString(KEY_USER, jsonUser);
            PlayerPrefs.Save();
        }

        public UserData GetUser()
        {
            if (PlayerPrefs.HasKey(KEY_USER))
            {
                string jsonUser = PlayerPrefs.GetString(KEY_USER);
                try
                {
                    //UserData userData = JsonUtility.FromJson<UserData>(jsonUser);
                    UserData userData = new UserData();
                    JsonUtility.FromJsonOverwrite(jsonUser, userData);
                    return userData;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return new UserData();
                }
            }
            else
            {
                return new UserData();
            }
        }
    }
}