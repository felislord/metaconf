using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference.Repository;
using UnityEngine;

namespace MetaConference
{
    [Serializable]
    [CreateAssetMenu(fileName = "Avatar", menuName = "Users/Create Avatar", order = 1)]
    public class AvatarData : ScriptableObject
    {
        public int Id = -1;
        public string Name;
        public string NamePrefab;
        public string NameVRPrefab;
        public GameObject Prefab;
        public GameObject VRPrefab;
        //public List<UserTagData> Tags = new List<UserTagData>();
        
        public Sprite FaceImage;
        public Sprite BodyImage;

        public AvatarData()
        {
        }
    }
}