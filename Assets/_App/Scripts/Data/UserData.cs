using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference.Repository;
using UnityEngine;

namespace MetaConference
{
    [CreateAssetMenu(fileName = "User", menuName = "Users/Create User", order = 1)]
    [Serializable]
    public class UserData : ScriptableObject
    {
        public int Id = -1;
        public string Name = "User";
        public string AvatarResPath = "";
        public AvatarData AvatarData;
        public List<UserTagData> Tags = new List<UserTagData>();

        public UserData()
        {
        }

        public bool IsValid() => Id != -1;
    }
}