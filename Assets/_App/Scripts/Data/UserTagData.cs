﻿using UnityEngine;

namespace MetaConference
{
    [CreateAssetMenu(fileName = "Tag", menuName = "Users/Create Tag", order = 2)]
    public class UserTagData:ScriptableObject
    {
        public int Id = -1;
        public string Name = "IT";
    }
}