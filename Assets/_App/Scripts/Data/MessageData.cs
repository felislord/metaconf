using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetaConference.Chat
{
    [Serializable]
    public class MessageData
    {
        public string UserId;
        public string UserName;
        public string Message;
    }
}