﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MetaConference
{
    public class TagsView : MonoBehaviour
    {
        [SerializeField] private TagCheckbox m_TagCheckboxPrefab;
        [SerializeField] private Transform m_Content;
        [SerializeField] private List<UserTagData> m_UserTags = new List<UserTagData>();

        private List<TagCheckbox> m_TagCheckboxes = new List<TagCheckbox>();

        public void InitCheckBoxes(UserData userData = null)
        {
            foreach (UserTagData userTagData in m_UserTags)
            {
                TagCheckbox tagCheckbox = Instantiate(m_TagCheckboxPrefab, m_Content);
                tagCheckbox.UserTag = userTagData;
                m_TagCheckboxes.Add(tagCheckbox);
                
                if (userData != null)
                {
                    InitCheckBox(userData, userTagData, tagCheckbox);
                }
            }
        }

        private void InitCheckBox(UserData userData, UserTagData userTagData, TagCheckbox tagCheckbox)
        {
            foreach (UserTagData requiredTagData in userData.Tags)
            {
                if (userTagData.Name.Contains(requiredTagData.Name))
                {
                    tagCheckbox.IsChecked = true;
                    break;
                }
            }
        }

        public List<UserTagData> GetCheckedTags()
        {
            return m_TagCheckboxes
                   .FindAll(checkbox => checkbox.IsChecked)
                   .Select(checkbox => checkbox.UserTag)
                   .Distinct()
                   .ToList();
        }

        public void UpdateUser(UserData userData)
        {
            List<UserTagData> checkedTags = GetCheckedTags();
            userData.Tags.Clear();
            userData.Tags.AddRange(checkedTags);
        }
    }
}