using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public class TagCheckbox : MonoBehaviour
    {
        [SerializeField] private Toggle m_Toggle;
        [SerializeField] private TextMeshProUGUI m_Title;
        
        private UserTagData m_UserTag;
        
        public bool IsChecked
        {
            get => m_Toggle.isOn;
            set => m_Toggle.isOn = value;
        }
        public UserTagData UserTag
        {
            get => m_UserTag;
            set
            {
                m_UserTag = value;
                m_Title.name = UserTag.Name;
            }
        }

        private void OnValidate()
        {
            if (m_Toggle == null)
            {
                TryGetComponent(out m_Toggle);
            }
        }
    }
}