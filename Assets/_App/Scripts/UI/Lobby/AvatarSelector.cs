﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public class AvatarSelector : MonoBehaviour
    {
        [SerializeField] private List<AvatarData> m_Avatars = new List<AvatarData>();
        [SerializeField] private Image m_FullBodyImage;

        private AvatarData m_CurrentAvatarData;
        private int m_AvatarCount = 0;
        private int m_CurrentPosition = 0;

        public AvatarData CurrentAvatar => m_CurrentAvatarData;

        private void Awake()
        {
            m_AvatarCount = m_Avatars.Count;
            //m_CurrentPosition = 0;
        }

        private void UpdateCurrentAvatar()
        {
            if (m_CurrentPosition >= m_AvatarCount)
            {
                m_CurrentPosition = 0;
            }

            if (m_CurrentPosition < 0)
            {
                m_CurrentPosition = m_AvatarCount - 1;
            }

            m_CurrentAvatarData = m_Avatars[m_CurrentPosition];
            m_FullBodyImage.sprite = m_CurrentAvatarData.BodyImage;
        }

        public void InitAvatar(string pathData)
        {
            AvatarData avatarData = Resources.Load<AvatarData>(pathData);
            if (avatarData != null)
            {
                m_CurrentPosition = m_Avatars.IndexOf(avatarData);
                if (m_CurrentPosition < 0)
                {
                    m_CurrentPosition = 0;
                }
            }

            UpdateCurrentAvatar();
        }

        public void Next()
        {
            m_CurrentPosition++;
            UpdateCurrentAvatar();
        }

        public void Prev()
        {
            m_CurrentPosition--;
            UpdateCurrentAvatar();
        }
    }
}