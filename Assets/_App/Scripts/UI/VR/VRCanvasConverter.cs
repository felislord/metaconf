﻿using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit.UI;

namespace MetaConference.VR
{
    public class VRCanvasConverter : MonoBehaviour
    {
        [SerializeField] private Canvas m_Canvas;
        [SerializeField] private Image m_BackgroundImage;
        [SerializeField] private float m_Width = 1400f;
        [SerializeField] private float m_Height = 1200f;
        [SerializeField] private float m_Scale = 0.0006f;

        private RectTransform m_RectTransform;
        private TrackedDeviceGraphicRaycaster m_TrackedDeviceGraphicRaycaster;
        private float m_InitWidth;
        private float m_InitHeight;
        private float m_InitScale;
        
        private void OnValidate()
        {
            if (m_Canvas == null)
            {
                TryGetComponent(out m_Canvas);
            }
        }

        private void Awake()
        {
            m_Canvas.TryGetComponent(out m_RectTransform);
            m_Canvas.TryGetComponent(out m_TrackedDeviceGraphicRaycaster);
            if (!m_TrackedDeviceGraphicRaycaster)
            {
                m_TrackedDeviceGraphicRaycaster = transform.AddComponent<TrackedDeviceGraphicRaycaster>();
            }
        }

        private void Start()
        {
            m_InitWidth = m_RectTransform.rect.width;
            m_InitHeight = m_RectTransform.rect.height;
            m_InitScale = m_RectTransform.localScale.x;
        }

        public void ConvertToWorld()
        {
            m_BackgroundImage.gameObject.SetActive(true);
            m_Canvas.renderMode = RenderMode.WorldSpace;

            m_RectTransform.sizeDelta = new Vector2(m_Width, m_Height);
            m_RectTransform.localScale = new Vector3(m_Scale, m_Scale, m_Scale);
            m_RectTransform.ForceUpdateRectTransforms();
        }
    }
}