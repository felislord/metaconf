using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference.VR;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public class UserItemView : BaseItemViewHolder<UserData>
    {
        [SerializeField] private Image m_FaceImage;
        [SerializeField] private TextMeshProUGUI m_NameText;
        [SerializeField] private TextMeshProUGUI m_TagsText;

        public override void Bind(UserData userData)
        {
            base.Bind(userData);
            m_NameText.text = userData.Name;
            string tags = String.Empty;
            if (userData.Tags.Count > 0)
            {
                foreach (UserTagData tagData in userData.Tags)
                {
                    tags += $"#{tagData.Name} ";
                }

                m_TagsText.text = tags;
            }
            else
            {
                m_TagsText.text = "No tags";
            }

            AvatarData avatarData = userData.AvatarData;
            if (avatarData != null)
            {
                m_FaceImage.sprite = userData.AvatarData.FaceImage;
            }
        }
    }
}