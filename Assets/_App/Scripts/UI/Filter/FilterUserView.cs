using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MetaConference.Chat;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace MetaConference
{
    public class FilterUserView : MonoBehaviour
    {
        [Header("UI")]
        [SerializeField] private TMP_Dropdown m_TagDropdown;
        [SerializeField] private FilterUserScrollView m_UserScrollView;

        [Space]
        [Header("Managers")]
        [SerializeField] private UserTagManager m_TagManager;
        [SerializeField] private AvatarManager m_AvatarManager;


        #region MonoBehaviour

        private void OnValidate()
        {
            if (!m_TagDropdown)
            {
                m_TagDropdown = GetComponentInChildren<TMP_Dropdown>();
            }
            
            if (!m_UserScrollView)
            {
                m_UserScrollView = GetComponentInChildren<FilterUserScrollView>();
            }

            if (!m_TagManager)
            {
                m_TagManager = FindObjectOfType<UserTagManager>();
            }

            if (!m_AvatarManager)
            {
                m_AvatarManager = FindObjectOfType<AvatarManager>();
            }
        }

        private void Start()
        {
            List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
            TMP_Dropdown.OptionData allTagData = new TMP_Dropdown.OptionData("All");
            options.Add(allTagData);

            foreach (UserTagData userTagData in m_TagManager.Tags)
            {
                TMP_Dropdown.OptionData optionData = new TMP_Dropdown.OptionData(userTagData.Name);
                options.Add(optionData);
            }

            m_TagDropdown.options = options;
            m_TagDropdown.onValueChanged.AddListener(SelectTag);

            InitFilterUsers();
        }

        #endregion

        private void InitFilterUsers()
        {
            List<UserData> users = m_AvatarManager.Avatars
                                                  .Select(npcController => npcController.UserData)
                                                  .Distinct()
                                                  .ToList();
            
            m_UserScrollView.Clear();
            m_UserScrollView.AddItems(users);
        }
        

        private void FilterUsers(UserTagData filterTagData)
        {
            List<UserData> filterUsers = m_AvatarManager.FilterPlayers(filterTagData);

            m_UserScrollView.Clear();
            m_UserScrollView.AddItems(filterUsers);
        }

        public void SelectTag(int position)
        {
            if (position == 0)
            {
                List<UserData> filterUsers = m_AvatarManager.FilterPlayers();

                m_UserScrollView.Clear();
                m_UserScrollView.AddItems(filterUsers);
            }
            else
            {
                UserTagData tagData = m_TagManager.Tags[position - 1];
                FilterUsers(tagData);
            }
        }
    }
}