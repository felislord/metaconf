﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public abstract class BaseScrollView<TData, TViewHolder> : MonoBehaviour
        where TViewHolder : BaseItemViewHolder<TData>
    {
        [SerializeField] protected TViewHolder ItemViewHolder;
        [SerializeField] protected ScrollRect ScrollView;

        protected List<TData> Items = new List<TData>();
        protected List<TViewHolder> ItemViewHolders = new List<TViewHolder>();

        public virtual void Clear()
        {
            foreach (TViewHolder viewHolderItem in ItemViewHolders)
            {
                Destroy(viewHolderItem.gameObject);
            }
            ItemViewHolders.Clear();
        }

        public virtual void AddItems(List<TData> items)
        {
            Items.AddRange(items);
            
            foreach (TData item in items)
            {
                AddItem(item);
            }
        }

        public virtual void AddItem(TData item)
        {
            Items.Add(item);
            
            TViewHolder newViewHolder = Instantiate(ItemViewHolder, ScrollView.content);
            newViewHolder.Bind(item);
            ItemViewHolders.Add(newViewHolder);
        }
    }
}