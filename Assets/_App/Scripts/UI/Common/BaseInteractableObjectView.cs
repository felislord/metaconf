using System.Collections;
using System.Collections.Generic;
using MetaConference.VR;
using TMPro;
using UnityEngine;

namespace MetaConference
{
    public abstract class BaseInteractableObjectView : MonoBehaviour, IViewVisibility
    {
        protected bool IsVisible = false;

        public void SetVisibility(bool isVisible)
        {
            gameObject.SetActive(isVisible);
        }

        public void SwitchVisibility()
        {
            IsVisible = !IsVisible;
            SetVisibility(IsVisible);
        }
    }
}