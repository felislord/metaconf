﻿using MetaConference.VR;
using UnityEngine;

namespace MetaConference
{
    public abstract class BaseItemViewHolder<T>: MonoBehaviour
    {
        protected T ItemData;

        public virtual void Bind(T data)
        {
            ItemData = data;
        }
    }
}