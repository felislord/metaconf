﻿namespace MetaConference.VR
{
    public interface IViewVisibility
    {
        public void SetVisibility(bool isVisible);
        public void SwitchVisibility();
    }
}