using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public class UserCardView : MonoBehaviour
    {
        [SerializeField] private Image m_FaceImage;
        [SerializeField] private TextMeshProUGUI m_NameText;
        [SerializeField] private TextMeshProUGUI m_TagsText;

        private UserData m_UserData;

        public void SetData(UserData userData)
        {
            m_UserData = userData;
            string userName = m_UserData.Name;
            userName = userName.Replace(" ", "\n");
            m_NameText.text = userName;
            string tags = String.Empty;
            if (m_UserData.Tags.Count > 0)
            {
                tags = "Tags:";
                foreach (UserTagData tagData in m_UserData.Tags)
                {
                    tags += $"\n- {tagData.Name}";
                }

                m_TagsText.text = tags;
            }
            else
            {
                m_TagsText.text = "No tags";
            }

            AvatarData avatarData = m_UserData.AvatarData;
            if (avatarData != null)
            {
                m_FaceImage.sprite = m_UserData.AvatarData.FaceImage;
            }
        }
    }
}