﻿using System;
using MetaConference.VR;
using UnityEngine;

namespace MetaConference
{
    public class AvatarView : MonoBehaviour, IViewVisibility
    {
        [SerializeField] private UserCardView m_UserCardView;

        private UserData m_UserData;
        private bool m_IsVisibleUserCard = false;

        #region Get Set

        public PlayerAvatarController AvatarController { private get; set; }

        public UserData UserData
        {
            get => m_UserData;
            set
            {
                m_UserData = value;
                m_UserCardView.SetData(m_UserData);
            }
        }

        #endregion

        #region MonoBehaviour

        private void OnValidate()
        {
            if (m_UserCardView == null)
            {
                m_UserCardView = GetComponentInChildren<UserCardView>(true);
            }
        }

        #endregion

        public void SwitchVisibility()
        {
            m_IsVisibleUserCard = !m_IsVisibleUserCard;
            SetVisibility(m_IsVisibleUserCard);
        }

        public void SetVisibility(bool isVisible)
        {
            //m_UserCardView.gameObject.SetActive(isVisible);
            gameObject.SetActive(m_IsVisibleUserCard);
        }
    }
}