using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference.Chat;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public class MessageViewHolder : BaseItemViewHolder<MessageData>
    {
        [SerializeField] private TextMeshProUGUI m_UserNameText;
        [SerializeField] private TextMeshProUGUI m_MessageText;

        public override void Bind(MessageData messageData)
        {
            base.Bind(messageData);
            m_UserNameText.text = messageData.UserName;
            m_MessageText.text = messageData.Message;
        }
    }
}
