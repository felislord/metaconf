using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference.Chat;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace MetaConference
{
    public class MessagesScrollView : BaseScrollView<MessageData, MessageViewHolder>
    {
        [SerializeField] private bool m_ScrollToBottom = false;

        #region MonoBehaviour

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private void OnEnable()
        {
            StartCoroutine(ForceUpdateScrollView());
        }

        #endregion


        public override void AddItem(MessageData messageData)
        {
            base.AddItem(messageData);
            if (isActiveAndEnabled)
            {
                StartCoroutine(ForceUpdateScrollView());
            }
        }

        private IEnumerator ForceUpdateScrollView()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(ScrollView.content);
            yield return new WaitForEndOfFrame();
            if (m_ScrollToBottom)
            {
                ScrollView.verticalNormalizedPosition = 0f;
            }
        }
    }
}