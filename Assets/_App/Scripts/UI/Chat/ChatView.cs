using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference.Chat;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace MetaConference
{
    public class ChatView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_ChatTitleText;
        [SerializeField] private MessagesScrollView m_MessagesView;
        [SerializeField] private InputMessageView m_InputMessageView;

        public string UserName { get; set; }
        

        public void SetTitle(string chatTitle)
        {
            if (m_ChatTitleText)
            {
                m_ChatTitleText.text = chatTitle;
            }
        }

        public void SendUserMessage(string message)
        {
            MessageData messageData = new MessageData()
            {
                UserId = UserName,
                UserName = UserName,
                Message = message
            };
            
            m_MessagesView.AddItem(messageData);
        }

        public void GetUserMessage(MessageData messageData)
        {
            m_MessagesView.AddItem(messageData);
        }
    }
}