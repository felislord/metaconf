using System;
using System.Collections;
using System.Collections.Generic;
using Michsky.MUIP;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MetaConference.Chat
{
    public class InputMessageView : MonoBehaviour
    {
        [SerializeField] private TMP_InputField m_InputMessage;
        [SerializeField] private ButtonManager m_SendButton;

        public UnityEvent<string> OnSend;

        private void Awake()
        {
            if (m_SendButton)
            {
                m_SendButton.onClick.AddListener(Send);
                m_SendButton.isInteractable = false;
            }

            if (m_InputMessage)
            {
                m_InputMessage.text = "";
                m_InputMessage.onValueChanged.AddListener(message =>
                {
                    if (!IsValidMessage())
                    {
                        m_SendButton.isInteractable = false;
                    }
                    else
                    {
                        m_SendButton.isInteractable = true;
                    }
                });
            }
        }

        private void Update()
        {
            OnEnterSend();
        }

        private void OnEnterSend()
        {
            if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
            {
                if (IsValidMessage())
                {
                    Send();
                }
            }
        }

        private bool IsValidMessage()
        {
            string message = m_InputMessage.text;
            return !string.IsNullOrEmpty(message);
        }

        public void Send()
        {
            if (!IsValidMessage())
            {
                return;
            }
            
            string message = m_InputMessage.text;
            m_InputMessage.text = "";
            OnSend?.Invoke(message);
        }
    }
}
