using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference.Chat;
using MetaConference.Repository;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace MetaConference
{
    public class GameSession : MonoBehaviourPunCallbacks
    {
        [SerializeField] private LocalRepository m_LocalRepository;
        [SerializeField] private InputPlayerManager m_PlayerInput;
        [SerializeField] private ChatSession m_ChatSession;
        [SerializeField] private AvatarManager m_AvatarManager;
        [SerializeField] private AvatarGenerator m_AvatarGenerator;

        //private Hashtable m_Hashtable = new Hashtable();
        private PlayerAvatarController m_MineAvatar;
        private UserData m_UserData;

        public PlayerAvatarController MineAvatar
        {
            get => m_MineAvatar;
            set => m_MineAvatar = value;
        }

        #region MonoBehaviour

        private void Awake()
        {
            if (!PhotonNetwork.IsConnected)
            {
                LoadLauncherScene();
            }
        }

        private void Start()
        {
            InitInputManager();
            InitMineAvatar();
            InitChat();
        }

        #endregion

        private void InitInputManager()
        {
            m_PlayerInput.EnableInput();
        }

        private void InitMineAvatar()
        {
            if (MineAvatar != null)
            {
                return;
            }

            m_UserData = m_LocalRepository.GetUser();
            m_AvatarGenerator.CreatePlayerAvatar(m_UserData, m_PlayerInput.InputPlatform, out m_MineAvatar);
            m_MineAvatar.UserData = m_UserData;
            m_MineAvatar.AvatarTransform.InputPlayerManager = m_PlayerInput;
            m_MineAvatar.AvatarInteractable.InputPlayerManager = m_PlayerInput;
            m_MineAvatar.IsMine = true;
            
            m_AvatarManager.ConnectPlayer(m_MineAvatar);
        }

        private void InitChat()
        {
            m_ChatSession.Initialize(PhotonNetwork.LocalPlayer);
        }
        
        private void LoadLauncherScene()
        {
            SceneManager.LoadScene(0);
        }

        public void BackToLaunch()
        {
            m_AvatarManager.DisconnectPlayer(m_MineAvatar);
            PhotonNetwork.Disconnect();
        }

        #region Photon

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
            //TODO
        }

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            base.OnMasterClientSwitched(newMasterClient);
            //TODO
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            m_AvatarManager.DisconnectPlayer(m_MineAvatar);
            LoadLauncherScene();
        }

        #endregion
    }
}