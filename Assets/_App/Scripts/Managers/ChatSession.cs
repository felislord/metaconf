using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Chat;
using Photon.Chat.Demo;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using AuthenticationValues = Photon.Chat.AuthenticationValues;
using Random = UnityEngine.Random;

namespace MetaConference.Chat
{
    public class ChatSession : MonoBehaviour, IChatClientListener
    {
        [Header("Chat")]
        [SerializeField] private string m_UserName = "";
        [SerializeField] private string m_ChannelName;
        [Tooltip("How many missed messages to receive from history")]
        [SerializeField] private int m_HistoryLengthToFetch = 10;
        [Tooltip("Max messages for chat")]
        [SerializeField] private int m_ChatMessageLimit = 0;
        [Tooltip("Max messages for channel")]
        [SerializeField] private int m_ChannelMessageLimit = 1;


        [Space]
        [Header("Views")]
        [SerializeField] private ChatView m_ChatView;

        private ChatClient m_ChatClient;
        private ChatChannel m_ChatChannel;
        private ChatAppSettings m_ChatAppSettings;
        private Player m_Player;
        
        private void OnDestroy()
        {
            Disconnect();
        }

        private void OnApplicationQuit()
        {
            Disconnect();
        }

        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            if (string.IsNullOrEmpty(m_UserName))
            {
                m_UserName = $"User_{Random.Range(100, 999)}";
            }

            m_ChatView.UserName = m_UserName;
            CreateChat();
        }

        public void Initialize(Player player)
        {
            m_Player = player;
            m_UserName = m_Player.NickName;
            m_ChatView.UserName = m_Player.NickName;
            
            m_ChatView.SetTitle(m_ChannelName);
            CreateChat();
        }

        private void Update()
        {
            if (m_ChatClient != null)
            {
                m_ChatClient.Service();
            }
        }

        private void CreateChat()
        {
            m_ChatAppSettings = PhotonNetwork.PhotonServerSettings.AppSettings.GetChatSettings();

            m_ChatClient = new ChatClient(this);
            #if !UNITY_WEBGL
            m_ChatClient.UseBackgroundWorkerForSending = true;
            #endif
            m_ChatClient.AuthValues = new AuthenticationValues(m_UserName);
            m_ChatClient.MessageLimit = m_ChatMessageLimit;

            m_ChatClient.ConnectUsingSettings(m_ChatAppSettings);

            Debug.Log($"Connecting as: {m_UserName}");
            Debug.Log($"Chat ID: {m_ChatAppSettings.AppIdChat}");
        }
        
        private void GetChannel()
        {
            if (m_ChatChannel == null)
            {
                bool isExistChannel = m_ChatClient.TryGetChannel(m_ChannelName, out m_ChatChannel);
                if (!isExistChannel)
                {
                    Debug.Log($"Chat channel is not found");
                    return;
                }

                m_ChatChannel.MessageLimit = m_ChannelMessageLimit;
                //m_ChatChannel.TruncateMessages();
            }
        }

        private void Disconnect()
        {
            if (m_ChatClient != null)
            {
                m_ChatClient.Disconnect();
            }
        }

        #region IChatClientListener

        public void DebugReturn(DebugLevel level, string message)
        {
            if (level == DebugLevel.ERROR)
            {
                Debug.LogError($"{name}: {message}");
            }
            else
            {
                Debug.Log($"{name}: {message}");
            }
        }

        public void OnDisconnected()
        {
            Debug.Log($"{name}: OnDisconnected");
        }

        public void OnConnected()
        {
            Debug.Log($"{name}: OnConnected");
            m_ChatClient.Subscribe(m_ChannelName, m_HistoryLengthToFetch);
            m_ChatClient.SetOnlineStatus(ChatUserStatus.Online);

            //SendDebugMessages();
        }

        private void SendDebugMessages()
        {
            for (int i = 0; i < 155; i++)
            {
                SendChatMessage("Number = " + i);
            }
        }

        public void OnChatStateChange(ChatState state)
        {
            Debug.Log($"{name}: OnChatStateChange: {state}, {state.ToString()}");
        }

        /*
         * TODO: fix get messages
         * See ChatChannel.MessageLimit.
         * Current limit messages of CHANNEL = 1 to get only 1 message.
         * Users can lose messages because of bad connection / disconnect ...
         * We should get last N or ALL messages and select new messages. 
         */
        public void OnGetMessages(string channelName, string[] senders, object[] messages)
        {
            if (string.IsNullOrEmpty(channelName))
            {
                return;
            }

            if (m_ChannelName.Equals(channelName))
            {
                if (m_ChatChannel != null)
                {
                    string message = messages[0].ToString();
                    string sender = senders[0];
                    MessageData messageData = new MessageData()
                    {
                        Message = message,
                        UserName = sender
                    };
                    
                    m_ChatView.GetUserMessage(messageData);
                    Debug.Log($"{channelName}: {m_ChatChannel.ToStringMessages()}");
                }
            }
        }

        public void OnSendMessage(string message)
        {
            SendChatMessage(message);
        }

        private void SendChatMessage(string message)
        {
            m_ChatClient.PublishMessage(m_ChannelName, message);
        }

        public void OnPrivateMessage(string sender, object message, string channelName)
        {
            //TODO
        }

        public void OnSubscribed(string[] channels, bool[] results)
        {
            Debug.Log($"{name}: OnSubscribed: {string.Join(", ", channels)}");
            foreach (string channel in channels)
            {
                if (channel.Equals(m_ChannelName))
                {
                    GetChannel();
                    break;
                }
                //m_ChatClient.PublishMessage(channel, "joins the chat");
            }
        }

        public void OnUnsubscribed(string[] channels)
        {
            //TODO
        }

        public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
        {
            //TODO
        }

        public void OnUserSubscribed(string channel, string user)
        {
            //TODO
        }

        public void OnUserUnsubscribed(string channel, string user)
        {
            //TODO
        }

        #endregion
    }
}