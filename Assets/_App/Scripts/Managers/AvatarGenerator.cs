﻿using System;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace MetaConference
{
    public class AvatarGenerator : MonoBehaviourPunCallbacks
    {
        [SerializeField] private List<PlayerAvatarController> m_PlayerPrefabs = new List<PlayerAvatarController>();
        [SerializeField] private List<PlayerAvatarController> m_VrPlayerPrefabs = new List<PlayerAvatarController>();
        [SerializeField] private List<NpcAvatarController> m_NpcPrefabs = new List<NpcAvatarController>();
        [SerializeField] private int m_RandomClamp = 40;

        private string GetRandomPrefab()
        {
            int randomPosition = Random.Range(0, m_PlayerPrefabs.Count);
            PlayerAvatarController playerPrefab = m_PlayerPrefabs[randomPosition];
            return $"PC/{playerPrefab.name}";
        }
        
        private string GetRandomVrPrefab()
        {
            int randomPosition = Random.Range(0, m_VrPlayerPrefabs.Count);
            PlayerAvatarController playerPrefab = m_VrPlayerPrefabs[randomPosition];
            return $"VR/{playerPrefab.name}";
        }
        
        private string GetRandomNpcPrefab()
        {
            int randomPosition = Random.Range(0, m_NpcPrefabs.Count);
            NpcAvatarController npcPrefab = m_NpcPrefabs[randomPosition];
            return $"NPC/{npcPrefab.name}";
        }

        private Vector3 GetRandomPosition()
        {
            float positionX = Random.Range(-m_RandomClamp, m_RandomClamp);
            float positionZ = Random.Range(-m_RandomClamp, m_RandomClamp);
            return new Vector3(positionX, 0.0f, positionZ);
        }

        private Quaternion GetRandomQuaternion()
        {
            float rotationY = Random.Range(0, 360);
            return Quaternion.Euler(0, rotationY, 0);
        }
        
        private PlayerAvatarController InstantiateAvatar(string prefabName, Vector3 position, Quaternion rotation)
        {
            GameObject avatarGameObject = PhotonNetwork.Instantiate(prefabName, position, rotation);
            avatarGameObject.TryGetComponent(out PlayerAvatarController photonPlayer);
            return photonPlayer;
        }

        public void CreatePlayerAvatar(UserData userData, InputPlayerPlatform inputPlayerPlatform, out PlayerAvatarController playerAvatar)
        {
            AvatarData avatarData = Resources.Load<AvatarData>(userData.AvatarResPath);
            if (avatarData == null)
            {
                playerAvatar = null;
                return;
            }

            string prefabResourceName = String.Empty;
            switch (inputPlayerPlatform)
            {
                case InputPlayerPlatform.PC:
                case InputPlayerPlatform.MOBILE:
                case InputPlayerPlatform.WEBGL:
                    prefabResourceName = $"PC/{avatarData.Prefab.name}";
                    break;
                case InputPlayerPlatform.VR:
                    prefabResourceName = $"VR/{avatarData.VRPrefab.name}";
                    break;
            }

            playerAvatar = InstantiateAvatar(
                prefabResourceName,
                Vector3.zero, 
                Quaternion.identity);
        }
        
        public void CreateRandomPlayerAvatar()
        {
            PlayerAvatarController photonPlayer = InstantiateAvatar(
                GetRandomPrefab(),
                GetRandomPosition(),
                GetRandomQuaternion());
        }
        
        public void CreateNpcAvatar()
        {
            BaseAvatarController npcAvatarController = InstantiateAvatar(
                GetRandomNpcPrefab(),
                GetRandomPosition(),
                GetRandomQuaternion());
        }
        
        public void CreateNpcAvatar(int npcCount)
        {
            for (int i = 0; i < npcCount; i++)
            {
                CreateNpcAvatar();
            }
        }
    }
}