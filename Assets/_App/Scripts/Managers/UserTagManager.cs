﻿using System.Collections.Generic;
using UnityEngine;

namespace MetaConference
{
    public class UserTagManager : MonoBehaviour
    {
        [SerializeField] private List<UserTagData> m_Tags = new List<UserTagData>();
        
        public List<UserTagData> Tags => m_Tags;

        public bool CompareTags(UserTagData tagData, UserTagData requiredTagData)
        {
            return tagData == requiredTagData;
        }
    }
}