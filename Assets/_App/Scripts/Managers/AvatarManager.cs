﻿using System;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;

namespace MetaConference
{
    public class AvatarManager : MonoBehaviourPunCallbacks
    {
        [SerializeField] private UserTagManager m_TagManager;
        
        private List<PlayerAvatarController> m_Players = new List<PlayerAvatarController>();
        private List<NpcAvatarController> m_NPCs = new List<NpcAvatarController>();
        private List<BaseAvatarController> m_Avatars = new List<BaseAvatarController>();
        
        public UnityEvent<BaseAvatarController> OnAvatarConnected;
        public UnityEvent<BaseAvatarController> OnAvatarDisconnected;
        
        public List<PlayerAvatarController> PlayerAvatarControllers => m_Players;
        public List<NpcAvatarController> NPCs => m_NPCs;
        public List<BaseAvatarController> Avatars => m_Avatars;
        public UserTagManager TagManager => m_TagManager;

        private void Awake()
        {
            //TODO
            
            m_NPCs.AddRange(FindObjectsOfType<NpcAvatarController>());
            m_Players.AddRange(FindObjectsOfType<PlayerAvatarController>());

            m_Avatars.AddRange(m_Players);
            m_Avatars.AddRange(m_NPCs);
        }

        [PunRPC]
        public void ConnectPlayer(PlayerAvatarController playerAvatarController)
        {
            OnAvatarConnected.Invoke(playerAvatarController);
            
            m_Avatars.Add(playerAvatarController);
            m_Avatars.Add(playerAvatarController);
        }

        [PunRPC]
        public void DisconnectPlayer(PlayerAvatarController playerAvatarController)
        {
            OnAvatarDisconnected?.Invoke(playerAvatarController);

            m_Players.Remove(playerAvatarController);
            m_Avatars.Remove(playerAvatarController);
        }
        
        public List<UserData> FilterPlayers(UserTagData filterTagData)
        {
            List<UserData> filterUsers = new List<UserData>();

            //foreach (NpcAvatarController npcAvatarController in m_NPCs)
            foreach (BaseAvatarController avatarController in m_Avatars)
            {
                UserData userData = avatarController.UserData;

                if (userData.Tags.Count == 0)
                {
                    avatarController.SetVisibility(false);
                    continue;
                }

                foreach (UserTagData userTagData in userData.Tags)
                {
                    if (m_TagManager.CompareTags(userTagData, filterTagData))
                    {
                        avatarController.SetVisibility(true);
                        filterUsers.Add(userData);
                        break;
                    }
                    avatarController.SetVisibility(false);
                }
            }

            return filterUsers;
        }

        public List<UserData> FilterPlayers()
        {
            foreach (BaseAvatarController avatarController in m_Avatars)
            {
                avatarController.SetVisibility(true);
            }
            
            List<UserData> users = m_Avatars
                                   .Select(npcController => npcController.UserData)
                                   .Distinct()
                                   .ToList();
            return users;
        }
    }
}