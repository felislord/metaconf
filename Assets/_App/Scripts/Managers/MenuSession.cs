using System;
using System.Collections;
using System.Collections.Generic;
using MetaConference;
using MetaConference.Repository;
using MetaConference.Utils;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public class MenuSession : MonoBehaviourPunCallbacks
    {
        [Header("Components")]
        [SerializeField] private LocalRepository m_LocalRepository;
        [SerializeField] private AvatarSelector m_AvatarSelector;
        [SerializeField] private TMP_InputField m_NameInputField;
        [SerializeField] private Button m_ConnectButton;

        [Header("Photon")]
        [SerializeField] private string m_SceneName = "AvatarsRoomVR";
        [SerializeField] private byte m_MaxPlayersPerRoom = 12;
        [SerializeField] private string m_RoomName = "Avatars";
        [SerializeField] private string m_GameVersion = "1";

        private bool m_IsConnecting = false;
        private UserData m_UserData = null;

        private void Awake()
        {
            if (Application.isMobilePlatform)
            {
                Application.targetFrameRate = 60;
            }
        }

        private void Start()
        {
            m_ConnectButton.onClick.AddListener(Connect);
            
            m_UserData = m_LocalRepository.GetUser();
            
            m_NameInputField.text = m_UserData.Name;
            m_AvatarSelector.InitAvatar(m_UserData.AvatarResPath);
        }

        private void SaveUser()
        {
            AvatarData avatarData = m_AvatarSelector.CurrentAvatar;
            m_UserData.AvatarData = avatarData;
            m_UserData.AvatarResPath = ResourcePathHelper.AVATAR_PATH + avatarData.Name;
            m_UserData.Name = m_NameInputField.text;

            m_LocalRepository.SaveUser(m_UserData);
        }
        
        #region Photon

        public void Connect()
        {
            SaveUser();
            
            m_IsConnecting = true;
            m_ConnectButton.interactable = false;

            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                PhotonNetwork.NickName = m_UserData.Name;
                PhotonNetwork.AutomaticallySyncScene = true;
                PhotonNetwork.OfflineMode = false;
                PhotonNetwork.GameVersion = m_GameVersion;
                //PhotonNetwork.UseRpcMonoBehaviourCache = true;
                
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            if (m_IsConnecting)
            {
                Debug.Log($"{name}: OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room.\n Calling: PhotonNetwork.JoinRandomRoom(); Operation will fail if no room found");
                PhotonNetwork.JoinRandomRoom();
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);
            Debug.Log($"{name}: Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

            PhotonNetwork.CreateRoom(m_RoomName,
                                     new RoomOptions()
                                     {
                                         MaxPlayers = m_MaxPlayersPerRoom,
                                     });
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            Debug.LogError($"{name}: Launcher:Disconnected");

            m_IsConnecting = false;
            m_ConnectButton.interactable = true;
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            Debug.Log($"{name}: Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.\nFrom here on, your game would be running.");
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                PhotonNetwork.LoadLevel(m_SceneName);
            }
        }

        #endregion
    }
}