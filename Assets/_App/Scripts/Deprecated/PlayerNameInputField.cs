using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MetaConference
{

    [RequireComponent(typeof(TMP_InputField))]
    public class PlayerNameInputField : MonoBehaviour
    {
        private const string PlayerNamePrefKey = "PlayerName";
        
        [SerializeField] private TMP_InputField m_NameInputField;

        private void OnValidate()
        {
            if (!m_NameInputField)
            {
                TryGetComponent(out m_NameInputField);
            }
        }

        private void Start()
        {
            string nickName = String.Empty;
            m_NameInputField.onValueChanged.AddListener(SetPlayerName);
            if (PlayerPrefs.HasKey(PlayerNamePrefKey))
            {
                nickName = PlayerPrefs.GetString(PlayerNamePrefKey);
                m_NameInputField.text = nickName;
            }
            else
            {
                nickName = $"User {Random.Range(1000, 10000)}";
            }

            PhotonNetwork.NickName = nickName;
        }
        
        public void SetPlayerName(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                Debug.LogError("Player Name is null or empty");
                return;
            }
            PhotonNetwork.NickName = value;
            PlayerPrefs.SetString(PlayerNamePrefKey, value);
        }
    }
}