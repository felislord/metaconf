using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace MetaConference
{
    public class StudentBody : PlayerAvatarController
    {
        private Transform headVR;
        private Transform leftHandVR;
        private Transform rightHandVR;

        [SerializeField] private Transform head;
        [SerializeField] private Transform leftHand;
        [SerializeField] private Transform rightHand;

        PhotonView pv;

        private XROrigin xrOrigin;

        
        
        public  bool IsMine
        {
            set
            {
                base.IsMine = value;
                if (photonView.IsMine)
                {
                    xrOrigin = FindObjectOfType<XROrigin>(true);
                    xrOrigin.gameObject.SetActive(true);

                    if (xrOrigin != null)
                    {
                        headVR = xrOrigin.GetComponentInChildren<Camera>().transform;

                        ActionBasedController[] xrControllers =
                            xrOrigin.GetComponentsInChildren<ActionBasedController>();
                        leftHandVR = xrControllers[0].transform;
                        rightHandVR = xrControllers[1].transform;
                    }
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            pv = GetComponent<PhotonView>();
        }

        protected void Update()
        {
            if (pv.IsMine)
            {
                head.position = headVR.position;
                head.rotation = headVR.rotation;
                
                leftHand.position = leftHandVR.position;
                leftHand.rotation = leftHandVR.rotation;
                
                rightHand.position = rightHandVR.position;
                rightHand.rotation = rightHandVR.rotation;
            }
        }
    }
}