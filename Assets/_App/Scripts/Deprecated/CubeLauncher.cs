using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MetaConference
{
    public class CubeLauncher : MonoBehaviourPunCallbacks
    {
        [SerializeField] private Button m_ConnectButton;
        [SerializeField] private string m_SceneName = "AvatarsRoom";
        [SerializeField] private byte m_MaxPlayersPerRoom = 4;
        [SerializeField] private string m_RoomName = "Cubes";
        [SerializeField] private string m_GameVersion = "1";

        private bool m_IsConnecting = false;

        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
            m_ConnectButton.onClick.AddListener(Connect);
        }

        public void Connect()
        {
            m_IsConnecting = true;
            m_ConnectButton.interactable = false;

            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = m_GameVersion;
            }
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            if (m_IsConnecting)
            {
                Debug.Log($"{name}: OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room.\n Calling: PhotonNetwork.JoinRandomRoom(); Operation will fail if no room found");
                
                PhotonNetwork.JoinRandomRoom();
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);
            Debug.Log($"{name}: Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");
            
            PhotonNetwork.CreateRoom(m_RoomName,
                                     new RoomOptions()
                                     {
                                         MaxPlayers = m_MaxPlayersPerRoom,
                                     });
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            Debug.LogError($"{name}: Launcher:Disconnected");
            
            m_IsConnecting = false;
            m_ConnectButton.interactable = true;
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            Debug.Log($"{name}: Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.\nFrom here on, your game would be running.");
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                PhotonNetwork.LoadLevel(m_SceneName);
            }
        }
    }
}