﻿using System;
using MetaConference.VR;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Examples;
using UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation;

namespace MetaConference
{
    public class XRVRInput : BaseVRInput
    {
        [Header("XR VR")]
        [SerializeField] private XROrigin m_XROrigin;
        [SerializeField] private XRDeviceSimulator m_DeviceSimulator;
        [SerializeField] private XRInteractionManager m_XRInteractionManager;
        [SerializeField] private LocomotionSchemeManager m_LocomotionSchemeManager;
        [SerializeField] private bool m_UseDeviceSimulator = true;

        [Space]
        [Header("Canvas")]
        [SerializeField] private VRCanvasConverter m_VRCanvas;

        [Space]
        [Header("Input")]
        [SerializeField] InputActionProperty m_OpenUIAction;


        private bool m_IsOpenUI = false;
        
        public XROrigin XROrigin => m_XROrigin;

        private void OnValidate()
        {
            if (m_DeviceSimulator == null)
            {
                m_DeviceSimulator = FindObjectOfType<XRDeviceSimulator>(true);
            }

            if (m_XRInteractionManager == null)
            {
                m_XRInteractionManager = FindObjectOfType<XRInteractionManager>(true);
            }

            if (m_XROrigin == null)
            {
                m_XROrigin = FindObjectOfType<XROrigin>(true);
            }

            if (m_LocomotionSchemeManager == null)
            {
                if (m_XROrigin != null)
                {
                    m_XROrigin.TryGetComponent(out m_LocomotionSchemeManager);
                }
            }

            if (Head == null || LeftHand == null || RightHand == null)
            {
                if (m_LocomotionSchemeManager != null)
                {
                    Head = m_LocomotionSchemeManager.headForwardSource;
                    LeftHand = m_LocomotionSchemeManager.leftHandForwardSource;
                    RightHand = m_LocomotionSchemeManager.rightHandForwardSource;
                }
            }
        }

        private void OpenUI(InputAction.CallbackContext context)
        {
            m_IsOpenUI = !m_IsOpenUI;
            Debug.Log("Action = performed");
            m_VRCanvas.gameObject.SetActive(m_IsOpenUI);
            Vector3 position = new Vector3();
            Vector3 headPosition = Head.transform.position;
            position = headPosition + Head.forward * 0.8f;
            m_VRCanvas.transform.position = position;
            m_VRCanvas.transform.rotation = Head.transform.rotation;
        }

        private void Update()
        {
        }

        public void OnSelectEntered(SelectEnterEventArgs args)
        {
            SelectGameObject(args.interactableObject.transform);
        }

        public override void EnableInput(bool isEnable)
        {
            if (m_UseDeviceSimulator)
            {
                m_DeviceSimulator.gameObject.SetActive(isEnable);
            }
            else
            {
                m_DeviceSimulator.gameObject.SetActive(false);
            }

            XROrigin.gameObject.SetActive(isEnable);
            m_XRInteractionManager.gameObject.SetActive(isEnable);

            if (isEnable)
            {
                m_VRCanvas.gameObject.SetActive(false);
                m_VRCanvas.ConvertToWorld();
                m_OpenUIAction.action.performed += OpenUI;
            }
        }

        public override void SelectGameObject(Transform selectedTransform)
        {
            OnSelectGameObject?.Invoke(selectedTransform);
        }
    }
}