﻿using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace MetaConference
{
    public class KeyboardMouseInput : BasePlayerInput
    {
        private Camera m_Camera;
        
        private void Start()
        {
            m_Camera = Camera.main;
        }

        private void FixedUpdate()
        {
            KeyboardMoveInput();
        }

        private void Update()
        {
            MouseLookInput();
            Select();
        }

        private void Select()
        {
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                Ray ray = m_Camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit raycastHit))
                {
                    SelectGameObject(raycastHit.transform);
                }
            }
        }

        private void KeyboardMoveInput()
        {
            Vector2 direction = new Vector2(
                Input.GetAxis("Horizontal"),
                Input.GetAxis("Vertical"));

            MoveInput = direction;
        }

        private void MouseLookInput()
        {
            Vector2 direction = new Vector2(
                Input.GetAxis("Mouse X"),
                Input.GetAxis("Mouse Y"));

            LookInput = direction;
        }

        public override void EnableInput(bool isEnable)
        {
            gameObject.SetActive(isEnable);
        }

        public override void SelectGameObject(Transform selectedTransform)
        {
            OnSelectGameObject?.Invoke(selectedTransform);
        }
    }
}