﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace MetaConference
{
    /// <summary>
    /// Maybe we should add:
    /// - MOBILE_AR
    /// - WEBGL_PC
    /// - WEBGL_MOBILE
    /// </summary>
    public enum InputPlayerPlatform
    {
        PC,
        MOBILE,
        WEBGL,
        VR
    }

    public class InputPlayerManager : MonoBehaviour
    {
        [SerializeField] private InputPlayerPlatform m_InputPlatform = InputPlayerPlatform.PC;

        [Header("PC / Mobile / WebGL")]
        [SerializeField] private BasePlayerInput m_KeyboardMouseInput;
        [SerializeField] private BasePlayerInput m_TouchInput;

        [Header("VR")]
        [SerializeField] private BaseVRInput m_XRVRInput;
        //[SerializeField] private BaseVRInput m_OculusVRInput;

        private BasePlayerInput m_ActivePlayerInput;
        
        public InputPlayerPlatform InputPlatform => m_InputPlatform;

        private void DisableInputs()
        {
            m_KeyboardMouseInput.EnableInput(false);
            m_TouchInput.EnableInput(false);
            m_XRVRInput.EnableInput(false);
        }

        public void EnableInput()
        {
            EnableInput(InputPlatform);
        }
        
        public void EnableInput(InputPlayerPlatform inputPlayerPlatform)
        {
            DisableInputs();

            m_InputPlatform = inputPlayerPlatform;
            
            switch (InputPlatform)
            {
                case InputPlayerPlatform.PC:
                    m_ActivePlayerInput = m_KeyboardMouseInput;
                    m_KeyboardMouseInput.EnableInput(true);
                    break;
                case InputPlayerPlatform.MOBILE:
                    m_ActivePlayerInput = m_TouchInput;
                    m_TouchInput.EnableInput(true);
                    break;
                case InputPlayerPlatform.WEBGL:
                    m_ActivePlayerInput = m_TouchInput;
                    m_TouchInput.EnableInput(true);
                    break;
                case InputPlayerPlatform.VR:
                    m_ActivePlayerInput = m_XRVRInput;
                    m_XRVRInput.EnableInput(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public BasePlayerInput GetPlayerInput()
        {
            return m_ActivePlayerInput;
        }

        public BaseVRInput GetVRPlayerInput()
        {
            if (m_ActivePlayerInput is BaseVRInput basePlayerInput)
            {
                return basePlayerInput;
            }
            return null;
        }
    }
}