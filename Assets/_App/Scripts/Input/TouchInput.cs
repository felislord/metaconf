using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MetaConference
{
    public class TouchInput: BasePlayerInput
    {
        [SerializeField] private GameObject m_JoySticksView;

        private Camera m_Camera;
        
        private void Start()
        {
            m_Camera = Camera.main;
        }
        
        private void Update()
        {
            Select();
        }

        private void Select()
        {
            Vector2 touchPosition;
            
            if (Application.isEditor)
            {
                if (Input.GetMouseButtonDown(0)
                    && !EventSystem.current.IsPointerOverGameObject())
                {
                    touchPosition = Input.mousePosition;
                    CheckRay(touchPosition);
                    return;
                }
            }
            
            if (Input.touchCount > 0)
            {
                if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)
                    && Input.touches[0].phase == TouchPhase.Began)
                {
                    touchPosition = Input.touches[0].position;
                    CheckRay(touchPosition);
                    return;
                }
            }
        }

        private void CheckRay(Vector2 touchPosition)
        {
            Ray ray = m_Camera.ScreenPointToRay(touchPosition);
            //Gizmos.DrawRay(Camera.transform.position, ray.direction);
            if (Physics.Raycast(ray, out RaycastHit raycastHit))
            {
                SelectGameObject(raycastHit.transform);
            }
        }

        public void AnalogMoveInput(Vector2 moveInput)
        {
            MoveInput = moveInput;
        }

        public void AnalogLookInput(Vector2 lookInput)
        {
            LookInput = lookInput;
        }

        public override void EnableInput(bool isEnable)
        {
            gameObject.SetActive(isEnable);
            m_JoySticksView.SetActive(isEnable);
        }

        public override void SelectGameObject(Transform selectedTransform)
        {
            OnSelectGameObject?.Invoke(selectedTransform);
        }
    }
}