﻿using System;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.Events;

namespace MetaConference
{
    public abstract class BaseVRInput : BasePlayerInput
    {
        [Header("Transform")]
        [SerializeField] private Transform m_Head;
        [SerializeField] private Transform m_LeftHand;
        [SerializeField] private Transform m_RightHand;

        public Transform Head
        {
            get => m_Head;
            protected set => m_Head = value;
        }

        public Transform LeftHand
        {
            get => m_LeftHand;
            protected set => m_LeftHand = value;
        }

        public Transform RightHand
        {
            get => m_RightHand;
            protected set => m_RightHand = value;
        }
    }
}