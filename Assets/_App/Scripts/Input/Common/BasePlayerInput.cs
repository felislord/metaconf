﻿using System;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace MetaConference
{
    public abstract class BasePlayerInput : MonoBehaviour
    {
        #region Events
        [Space]
        public UnityEvent<Transform> OnSelectGameObject;
        public UnityEvent<Transform> OnHoverGameObject;

        #endregion

        #region Get Set

        public Vector2 MoveInput { get; protected set; }
        public Vector2 LookInput { get; protected set; }

        #endregion

        #region Methods

        public abstract void EnableInput(bool isEnable);
        public abstract void SelectGameObject(Transform selectedTransform);

        #endregion
    }
}