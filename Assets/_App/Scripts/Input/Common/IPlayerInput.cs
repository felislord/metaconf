﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MetaConference
{
    public interface IPlayerInput
    {
        public void EnableInput(bool isEnable);

        public void SelectGameObject(Transform selectedTransform);
    }
}