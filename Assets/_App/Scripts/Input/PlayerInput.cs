﻿using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace MetaConference
{
    public class PlayerInput : MonoBehaviour
    {
        public PlayerAvatarController Player;

        [SerializeField] private bool m_CursorInputForLook = true;
        private bool m_IsBlockKeyboardInput = false;
        
        public Vector3 MoveInput { get; private set; }
        public Vector2 LookInput { get; private set; }

        private void Awake()
        {
            Keyboard keyboard = Keyboard.current;
            Debug.Log($"{nameof(PlayerInput)}: keyboard = {keyboard}");
        }

        private void FixedUpdate()
        {
            KeyboardMoveInput();
            if (m_CursorInputForLook)
            {
                MouseLookInput();
            }
        }

        private void KeyboardMoveInput()
        {
            Vector3 direction = new Vector3(
                Input.GetAxis("Horizontal"),
                0,
                Input.GetAxis("Vertical"));

            if (!m_IsBlockKeyboardInput)
            {
                MoveInput = direction;
            }
            //Move(direction);
        }

        private void MouseLookInput()
        {
            Vector2 direction = new Vector2(
                Input.GetAxis("Mouse X"),
                Input.GetAxis("Mouse Y"));

            if (!m_IsBlockKeyboardInput)
            {
                LookInput = direction;
            } //Look(direction);
        }

        public void AnalogMoveInput(Vector2 moveInput)
        {
            Vector3 direction = new Vector3(moveInput.x, 0f, moveInput.y);
            Debug.Log($"{name}: analog move: {direction}");
            MoveInput = direction;
            m_IsBlockKeyboardInput = direction != Vector3.zero;
            //Move(direction);
        }

        public void AnalogLookInput(Vector2 lookInput)
        {
            LookInput = lookInput;
            m_IsBlockKeyboardInput = lookInput != Vector2.zero;
            //Look(lookInput);
        }

        private void Move(Vector3 direction)
        {
            if (Player != null)
            {
                //Player.Move(direction);
            }
        }

        private void Look(Vector2 direction)
        {
            if (Player != null)
            {
                //Player.Look(direction);
            }
        }
    }
}