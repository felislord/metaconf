﻿using System;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace MetaConference
{
    public abstract class DesktopPlayerInput : MonoBehaviour, IPlayerInput
    {
        [SerializeField] private CinemachineVirtualCamera m_VirtualCamera;
        [SerializeField] private Camera m_Camera;
        
        public UnityEvent<Transform> OnSelectGameObject;

        #region Get Set

        public CinemachineVirtualCamera VirtualCamera => m_VirtualCamera;
        public Camera Camera => m_Camera;
        public Vector2 MoveInput { get; protected set; }
        public Vector2 LookInput { get; protected set; }

        #endregion

        public abstract void EnableInput(bool isEnable);
        
        public abstract void SelectGameObject(Transform selectedTransform);
    }
}