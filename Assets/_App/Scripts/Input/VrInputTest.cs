﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Inputs;

namespace MetaConference
{
    public class VrInputTest : MonoBehaviour
    {
        [SerializeField]
        InputActionProperty m_SelectAction;
        /// <summary>
        /// The Input System action to use for selecting an Interactable.
        /// Must be an action with a button-like interaction where phase equals performed when pressed.
        /// Typically a <see cref="ButtonControl"/> Control or a Value type action with a Press or Sector interaction.
        /// </summary>
        /// <seealso cref="selectActionValue"/>
        public InputActionProperty selectAction
        {
            get => m_SelectAction;
            set => SetInputActionProperty(ref m_SelectAction, value);
        }
        
        [SerializeField]
        InputActionProperty m_SelectActionValue;
        /// <summary>
        /// The Input System action to read values for selecting an Interactable.
        /// Must be an <see cref="AxisControl"/> Control or <see cref="Vector2Control"/> Control.
        /// </summary>
        /// <remarks>
        /// Optional, Unity uses <see cref="selectAction"/> when not set.
        /// </remarks>
        /// <seealso cref="selectAction"/>
        public InputActionProperty selectActionValue
        {
            get => m_SelectActionValue;
            set => SetInputActionProperty(ref m_SelectActionValue, value);
        }

        private void Update()
        {
            Debug.Log("Action = " + m_SelectAction.action.phase);
            Debug.Log("ActionValue = " + m_SelectActionValue.action.phase);
        }


        void SetInputActionProperty(ref InputActionProperty property, InputActionProperty value)
        {
            if (Application.isPlaying)
            {
                property.DisableDirectAction();
            }

            property = value;

            if (Application.isPlaying && isActiveAndEnabled)
            {
                property.EnableDirectAction();
            }
        }
        
        void DisableAllDirectActions()
        {
            m_SelectAction.DisableDirectAction();
            m_SelectActionValue.DisableDirectAction();
        }
        
        void EnableAllDirectActions()
        {
            m_SelectAction.EnableDirectAction();
            m_SelectActionValue.EnableDirectAction();
        }

        public void PrintDebugText(string debugText)
        {
            Debug.Log("Debug: " + debugText);
        }
        
        public void OnFirstHoverEntered(HoverEnterEventArgs args)
        {
            Debug.Log($"{nameof(AvatarInteractable)}: OnFirstHoverEntered");
            Debug.Log($"{nameof(AvatarInteractable)}: OnFirstHoverEntered: args: " +
                      $" interactableObject: {args.interactableObject.transform.name}" +
                      $" interactorObject: {args.interactorObject.transform.name}");
        }

        public void OnLastHoverExited(HoverExitEventArgs args)
        {
            Debug.Log($"{nameof(AvatarInteractable)}: OnLastHoverExited");
        }

        public void OnFirstSelectEntered(SelectEnterEventArgs args)
        {
            Debug.Log($"{nameof(AvatarInteractable)}: OnFirstSelectEntered");
        }

        public void OnLastSelectExited(SelectExitEventArgs args)
        {
            Debug.Log($"{nameof(AvatarInteractable)}: OnLastSelectExited");
        }
    }
}