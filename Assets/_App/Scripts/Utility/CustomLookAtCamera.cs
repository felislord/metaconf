﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetaConference
{
    public class CustomLookAtCamera : MonoBehaviour
    {
        private enum FixAxe
        {
            X,
            Y,
            Z
        }

        [SerializeField] private bool m_IsLootAtCamera = false;
        [SerializeField] private FixAxe m_Axe = FixAxe.Y;
        [SerializeField] private bool m_UseSlerp = false;

        [SerializeField] private bool m_UseMainCamera = false;
        private Transform m_CameraTransform;

        private void Start()
        {
            m_CameraTransform = Camera.main.transform;
        }

        public void Update()
        {
            if (m_CameraTransform == null)
            {
                return;
            }

            if (m_IsLootAtCamera)
            {
                Quaternion rotation = Quaternion.identity;
                Vector3 direction = m_CameraTransform.position - transform.position;
                switch (m_Axe)
                {
                    case FixAxe.X:
                        direction.x = 0;
                        break;
                    case FixAxe.Y:
                        direction.y = 0;
                        break;
                    case FixAxe.Z:
                        direction.z = 0;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                rotation = Quaternion.LookRotation(direction);

                if (m_UseSlerp)
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.5f);
                }
                else
                {
                    transform.rotation = rotation;
                }
            }
        }
    }
}