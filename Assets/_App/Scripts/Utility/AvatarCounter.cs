using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace MetaConference
{
    public class AvatarCounter : MonoBehaviour
    {
        [SerializeField] private AvatarManager m_AvatarManager;
        [SerializeField] private TextMeshProUGUI m_CountText;
        [SerializeField] private float m_Delay = 1f;
        
        public void UpdateCount()
        {
            if (m_CountText)
            {
                int count = m_AvatarManager.Avatars.Count;
                m_CountText.text = $"Avatars: {count}";
            }
        }
    }
}