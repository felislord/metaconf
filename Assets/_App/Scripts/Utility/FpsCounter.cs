using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MetaConference
{
    public class FpsCounter : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_FpsText;

        private void OnValidate()
        {
            if (!m_FpsText)
            {
                TryGetComponent(out m_FpsText);
            }
        }

        private void Update()
        {
            if (m_FpsText)
            {
                int fps = (int)(1f / Time.unscaledDeltaTime);
                m_FpsText.text = $"FPS: {fps}";
            }
        }
    }
}