using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace MetaConference
{
    public class PlayerCounter : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_CountText;
        
        public void UpdateCount()
        {
            if (m_CountText)
            {
                int count = PhotonNetwork.PlayerList.Length;
                m_CountText.text = $"Players: {count}";
            }
        }
    }
}