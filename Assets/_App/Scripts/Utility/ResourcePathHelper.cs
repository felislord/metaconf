﻿namespace MetaConference.Utils
{
    public class ResourcePathHelper
    {
        public const string AVATAR_PATH = "Data/Avatars/";
        public const string SPRITE_PATH = "Sprites/";
        public const string PC_PREFAB_PATH = "Avatars/PC/";
        public const string VR_PREFAB_PATH = "Avatars/VR/";
    }
}