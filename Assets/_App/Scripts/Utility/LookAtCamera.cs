﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetaConference
{
    public class LookAtCamera : MonoBehaviour
    {
        public bool IsLootAtCamera = true;
        private Transform m_CameraTransform;

        private void OnEnable()
        {
            if (m_CameraTransform == null)
            {
                m_CameraTransform = Camera.main.transform;
            }
        }

        private void Start()
        {
            m_CameraTransform = Camera.main.transform;
        }

        public void Update()
        {
            if (IsLootAtCamera)
            {
                Vector3 targetPosition = new Vector3(m_CameraTransform.position.x,
                                                     transform.position.y,
                                                     m_CameraTransform.position.z);
                transform.LookAt(targetPosition);
            }
        }
    }
}