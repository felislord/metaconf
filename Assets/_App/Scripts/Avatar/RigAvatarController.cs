using UnityEngine;
using UnityEngine.Serialization;

namespace MetaConference
{
    [System.Serializable]
    public class MapTransform
    {
        public Transform VRTarget;
        public Transform IKTarget;

        [SerializeField] private Vector3 m_TrackingPositionOffset;
        [SerializeField] private Vector3 m_TrackingRotationOffset;

        public void MapVRAvatar()
        {
            if (VRTarget == null || IKTarget == null)
            {
                //return;
            }

            IKTarget.position = VRTarget.TransformPoint(m_TrackingPositionOffset);
            IKTarget.rotation = VRTarget.rotation * Quaternion.Euler(m_TrackingRotationOffset);
        }
    }
}