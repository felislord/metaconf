using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace MetaConference
{
    public class AvatarInteractable : BaseAvatarInteractable
    {
        private BasePlayerInput m_BasePlayerInput;
        
        public override InputPlayerManager InputPlayerManager
        {
            set
            {
                base.InputPlayerManager = value;

                m_BasePlayerInput = InputPlayerManager.GetPlayerInput();
                m_BasePlayerInput.OnSelectGameObject.AddListener(SelectGameObject);
            }
        }
    }
}