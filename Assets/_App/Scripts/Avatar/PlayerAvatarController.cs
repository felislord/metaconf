﻿using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;

namespace MetaConference
{
    public class PlayerAvatarController : BaseAvatarController
    {
        [SerializeField] private BaseAvatarTransform m_AvatarTransform;
        [SerializeField] private BaseAvatarInteractable m_AvatarInteractable;

        #region Get Set

        public BaseAvatarTransform AvatarTransform => m_AvatarTransform;
        public BaseAvatarInteractable AvatarInteractable
        {
            get => m_AvatarInteractable;
            set => m_AvatarInteractable = value;
        }

        #endregion

        #region MonoBehaviour

        protected override void OnValidate()
        {
            base.OnValidate();
            if (m_AvatarTransform == null)
            {
                m_AvatarTransform = GetComponent<BaseAvatarTransform>();
            }

            if (AvatarInteractable == null)
            {
                AvatarInteractable = GetComponent<BaseAvatarInteractable>();
            }
        }

        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);

            if (m_AvatarTransform)
            {
                m_AvatarTransform.AvatarController = this;
            }

            if (AvatarView)
            {
                AvatarView.AvatarController = this;
            }

            if (AvatarInteractable)
            {
                AvatarInteractable.AvatarController = this;
            }
        }
        
        protected override void Start()
        {
            base.Start();
            if (!IsMine && UserData != null)
            {
                AvatarView.UserData = UserData;
            }
        }

        #endregion
    }
}