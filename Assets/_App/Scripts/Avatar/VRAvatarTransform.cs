using System;
using System.Collections.Generic;
using MetaConference;
using Photon.Pun;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using UnityEngine.XR.Interaction.Toolkit;
using XRController = UnityEngine.InputSystem.XR.XRController;

namespace MetaConference
{
    public class VRAvatarTransform : BaseAvatarTransform
    {
        [SerializeField] private MapTransform m_Head;
        [SerializeField] private MapTransform m_LeftHand;
        [SerializeField] private MapTransform m_RightHand;

        [SerializeField] private float m_TurnSmoothness = 4f;

        [SerializeField] private Transform m_IKHead;
        [SerializeField] private Vector3 m_HeadBodyOffset;
        [SerializeField] private List<GameObject> m_HiddenGameObjects;
        
        private BaseVRInput m_PlayerInput;

        public override InputPlayerManager InputPlayerManager
        {
            set
            {
                base.InputPlayerManager = value;
                m_PlayerInput = InputPlayerManager.GetVRPlayerInput();
                
                m_Head.VRTarget= m_PlayerInput.Head;
                m_LeftHand.VRTarget= m_PlayerInput.LeftHand;
                m_RightHand.VRTarget= m_PlayerInput.RightHand;

                if (photonView.IsMine)
                {
                    m_HiddenGameObjects.ForEach(hiddenGameObject => hiddenGameObject.SetActive(false));
                }

                SkinnedMeshRenderer[] meshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
                foreach (SkinnedMeshRenderer meshRenderer in meshRenderers)
                {
                    meshRenderer.gameObject.SetActive(false);
                }
            }
        }

        protected void OnValidate()
        {
            if (m_Head.IKTarget == null || m_Head == null || m_LeftHand == null || m_RightHand == null)
            {
                if (transform.TryGetComponent(out RigBuilder rigBuilder))
                {
                    Transform rigParent = rigBuilder.layers[0].rig.transform;
                    TwoBoneIKConstraint[] twoBoneIKConstraints =
                        rigParent.GetComponentsInChildren<TwoBoneIKConstraint>();
                    MultiParentConstraint headIKConstraint = rigParent.GetComponentInChildren<MultiParentConstraint>();

                    if (twoBoneIKConstraints.Length >= 2)
                    {
                        m_LeftHand.IKTarget = twoBoneIKConstraints[0].data.target;
                        m_RightHand.IKTarget = twoBoneIKConstraints[1].data.target;
                    }

                    if (headIKConstraint != null)
                    {
                        m_Head.IKTarget = headIKConstraint.transform;
                        m_IKHead = headIKConstraint.transform;
                    }
                }
            }
        }

        protected void Update()
        {
            if (photonView.IsMine)
            {
                transform.position = m_IKHead.position + m_HeadBodyOffset;
                transform.forward = Vector3.Lerp(transform.forward,
                                                 Vector3.ProjectOnPlane(m_IKHead.forward, Vector3.up).normalized,
                                                 Time.deltaTime * m_TurnSmoothness);

                m_Head.MapVRAvatar();
                m_LeftHand.MapVRAvatar();
                m_RightHand.MapVRAvatar();
            }
        }
    }
}