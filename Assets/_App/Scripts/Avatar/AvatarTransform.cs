using System;
using Cinemachine;
using UnityEngine;

namespace MetaConference
{
    [RequireComponent(typeof(CharacterController))]
    public class AvatarTransform : BaseAvatarTransform
    {
        #region SerializeFields

        [Header("Player")]
        [SerializeField] private float m_MoveSpeed = 2.0f;
        [SerializeField] private float m_SpeedChangeRate = 10.0f;
        [Range(0.0f, 0.3f)] [SerializeField] private float m_RotationSmoothTime = 0.12f;

        [Header("Cinemachine")]
        [SerializeField] private CinemachineVirtualCamera m_CinemachineVirtualCamera;
        [SerializeField] private GameObject m_CinemachineCameraTarget;
        [SerializeField] private float m_TopClamp = 70.0f;
        [SerializeField] private float m_BottomClamp = -30.0f;

        #endregion

        #region Player

        private float m_Speed;
        private float m_AnimationBlend;
        private float m_TargetRotation = 0.0f;
        private float m_RotationVelocity;

        #endregion

        #region Animation Ids

        private int m_AnimIDSpeed;
        private int m_AnimIDMotionSpeed;

        #endregion

        #region Cinemachine

        private float m_CinemachineTargetYaw;
        private float m_CinemachineTargetPitch;

        #endregion

        private Animator m_Animator;
        private CharacterController m_CharacterController;
        private BasePlayerInput m_BasePlayerInput;
        private Camera m_MainCamera;

        #region Get Set

        public override InputPlayerManager InputPlayerManager
        {
            set
            {
                base.InputPlayerManager = value;
                m_BasePlayerInput = InputPlayerManager.GetPlayerInput();
                if (m_CinemachineVirtualCamera == null)
                {
                    m_CinemachineVirtualCamera = GetComponentInChildren<CinemachineVirtualCamera>();
                }
                if (m_CinemachineVirtualCamera != null)
                {
                    m_CinemachineVirtualCamera.gameObject.SetActive(true);
                    m_CinemachineVirtualCamera.Follow = m_CinemachineCameraTarget.transform;
                    m_CinemachineTargetYaw = m_CinemachineCameraTarget.transform.rotation.eulerAngles.y;
                }
            }
        }

        #endregion


        #region MonoBehaviour

        protected void Awake()
        {
            m_Animator = GetComponent<Animator>();
            m_CharacterController = GetComponent<CharacterController>();
        }

        protected void Start()
        {
            if (m_MainCamera == null)
            {
                m_MainCamera = Camera.main;
            }

            AssignAnimationIDs();
        }

        protected void Update()
        {
            if (m_BasePlayerInput != null)
            {
                Move(m_BasePlayerInput.MoveInput);
                Look(m_BasePlayerInput.LookInput);
            }
        }

        #endregion
        
        private void InitVirtualCamera()
        {
           
        }

        private void AssignAnimationIDs()
        {
            m_AnimIDSpeed = Animator.StringToHash("Speed");
            m_AnimIDMotionSpeed = Animator.StringToHash("MotionSpeed");
        }

        public void Look(Vector2 direction)
        {
            m_CinemachineTargetYaw += direction.x;
            m_CinemachineTargetPitch += direction.y;

            m_CinemachineTargetYaw = ClampAngle(m_CinemachineTargetYaw, float.MinValue, float.MaxValue);
            m_CinemachineTargetPitch = ClampAngle(m_CinemachineTargetPitch, m_BottomClamp, m_TopClamp);

            m_CinemachineCameraTarget.transform.rotation = Quaternion.Euler(
                m_CinemachineTargetPitch,
                m_CinemachineTargetYaw, 0.0f);
        }

        public void Move(Vector2 direction)
        {
            direction.Normalize();

            float targetSpeed = m_MoveSpeed;
            if (direction == Vector2.zero)
            {
                targetSpeed = 0.0f;
            }

            float currentHorizontalSpeed =
                new Vector3(m_CharacterController.velocity.x, 0.0f, m_CharacterController.velocity.z).magnitude;

            m_Speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed, Time.deltaTime * m_SpeedChangeRate);
            m_AnimationBlend = Mathf.Lerp(m_AnimationBlend, targetSpeed, Time.deltaTime * m_SpeedChangeRate);
            if (m_AnimationBlend < 0.01f)
            {
                m_AnimationBlend = 0f;
            }

            if (direction != Vector2.zero)
            {
                m_TargetRotation = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg +
                                   m_MainCamera.transform.eulerAngles.y;
                float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, m_TargetRotation,
                                                       ref m_RotationVelocity,
                                                       m_RotationSmoothTime);

                transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
            }

            Vector3 targetDirection = Quaternion.Euler(0.0f, m_TargetRotation, 0.0f) * Vector3.forward;
            m_CharacterController.Move(targetDirection.normalized * (m_Speed * Time.deltaTime));

            m_Animator.SetFloat(m_AnimIDSpeed, m_AnimationBlend);
            m_Animator.SetFloat(m_AnimIDMotionSpeed, 1);
        }

        private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }

        private void OnFootstep(AnimationEvent animationEvent)
        {
        }

        private void OnLand(AnimationEvent animationEvent)
        {
        }
    }
}