﻿using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;

namespace MetaConference
{
    public class NpcAvatarController : BaseAvatarController
    {
        #region MonoBehaviour

        protected override void Start()
        {
            base.Start();
            if (UserData != null)
            {
                AvatarView.UserData = UserData;
            }
        }

        #endregion
    }
}