using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace MetaConference
{
    public abstract class BaseAvatarInteractable : MonoBehaviour
    {
        private InputPlayerManager m_InputPlayerManager;
        
        public PlayerAvatarController AvatarController { private get; set; }
        
        public virtual InputPlayerManager InputPlayerManager
        {
            get => m_InputPlayerManager;
            set => m_InputPlayerManager = value;
        }

        public virtual void SelectGameObject(Transform selectedTransform)
        {
            if (selectedTransform.TryGetComponent(out BaseAvatarController avatarController))
            {
                avatarController.AvatarView.SwitchVisibility();
                return;
            }

            if (selectedTransform.tag.Equals("Prop"))
            {
                if (selectedTransform.TryGetComponent(out BaseInteractableObject interactableObject))
                {
                    interactableObject.View.SwitchVisibility();
                    return;
                }
            }
        }
    }
}