using System.Collections;
using System.Collections.Generic;
using MetaConference;
using Photon.Pun;
using UnityEngine;

namespace MetaConference
{
    public abstract class BaseAvatarTransform : MonoBehaviourPunCallbacks
    {
        private InputPlayerManager m_InputPlayerManager;
        
        public PlayerAvatarController AvatarController { private get; set; }
        
        public virtual InputPlayerManager InputPlayerManager
        {
            get => m_InputPlayerManager;
            set => m_InputPlayerManager = value;
        }
    }
}