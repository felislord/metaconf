﻿using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;

namespace MetaConference
{
    public abstract class BaseAvatarController : MonoBehaviourPunCallbacks
    {
        [SerializeField] private UserData m_UserData;
        [SerializeField] private AvatarView m_AvatarView;

        #region Get Set

        public bool IsMine { get; set; }
        public AvatarView AvatarView
        {
            get => m_AvatarView;
            set => m_AvatarView = value;
        }
        public UserData UserData
        {
            get => m_UserData;
            set
            {
                m_UserData = value;
                AvatarView.UserData = m_UserData;
            }
        }

        #endregion

        #region MonoBehaviour

        protected virtual void OnValidate()
        {
            if (AvatarView == null)
            {
                AvatarView = GetComponentInChildren<AvatarView>(true);
            }
        }

        protected virtual void Awake()
        {
            if (AvatarView)
            {
                //AvatarView.AvatarController = this;
            }
        }

        protected virtual void Start()
        {
        }

        #endregion

        public void SetVisibility(bool isVisible)
        {
            if (!IsMine)
            {
                gameObject.SetActive(isVisible);
            }
        }
    }
}