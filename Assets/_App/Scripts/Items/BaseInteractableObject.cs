using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MetaConference
{
    public abstract class BaseInteractableObject : MonoBehaviour
    {
        [SerializeField] private BaseInteractableObjectView m_InteractableObjectView;
        
        public BaseInteractableObjectView View
        {
            get => m_InteractableObjectView;
            private set => m_InteractableObjectView = value;
        }

        protected virtual void OnValidate()
        {
            if (m_InteractableObjectView == null)
            {
                m_InteractableObjectView = GetComponentInChildren<BaseInteractableObjectView>();
            }
        }
    }
}