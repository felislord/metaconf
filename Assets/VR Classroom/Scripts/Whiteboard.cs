﻿using System.Linq;
using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;

namespace ChiliGames.VRClassroom
{
    public class Whiteboard : MonoBehaviourPunCallbacks
    {
        public int TextureSize = 1024;

        public bool UseAlpha = false;

        //private int TextureSize = 2048;
        //private int TextureSize = 1024;
        private Texture2D texture;
        private Color[] whitePixels;
        new Renderer renderer;

        private Dictionary<int, MarkerData> markerIDs = new Dictionary<int, MarkerData>();

        class MarkerData
        {
            public Color[] color;
            public bool touchingLastFrame;
            public float[] pos;
            public int pensize;
            public float[] colorRGB;
            public float lastX;
            public float lastY;
        }

        [SerializeField] List<Renderer> otherWhiteboards;

        bool applyingTexture;

        [HideInInspector] public PhotonView pv;

        private float widh, height = 0;

        void Awake()
        {
            renderer = GetComponent<Renderer>();
            //texture = new Texture2D(TextureSize, TextureSize, TextureFormat.RGBA32, false);
            texture = new Texture2D(TextureSize, TextureSize, TextureFormat.RGBA32, false);
            texture.filterMode = FilterMode.Trilinear;
            texture.anisoLevel = 3;
            renderer.material.mainTexture = texture;

            foreach (var item in otherWhiteboards)
            {
                item.material.mainTexture = texture;
            }

            //texture.Apply();

            // pv = GetComponent<PhotonView>();

            //whitePixels = Enumerable.Repeat(Color.white, TextureSize * TextureSize).ToArray();
            if (!UseAlpha)
            {
                whitePixels = Enumerable.Repeat(Color.white, TextureSize * TextureSize).ToArray();
            }
            else
            {
                whitePixels = Enumerable.Repeat(Color.clear, TextureSize * TextureSize).ToArray();
            }

            //texture.SetPixels(whitePixels);

            //TODO - костыль, чтобы доска очищалась только один раз
            if (PhotonNetwork.IsMasterClient)
            {
                texture.SetPixels(whitePixels);
            }

            texture.Apply();

            pv = GetComponent<PhotonView>();


            widh = renderer.material.mainTexture.width;
            height = renderer.material.mainTexture.height;

            //if (UseAlpha)
            {
                //texture.SetPixels(whitePixels);
                //texture.Apply();

                //ClearWhiteboard();
            }
        }

        //RPC sent by the Marker class so every user gets the information to draw in whiteboard.
        [PunRPC]
        public void DrawAtPosition(int id, float[] _pos, int _pensize, float[] _color)
        {
            if (id < 0)
            {
                return;
            }

            try
            {
                if (!markerIDs.ContainsKey(id))
                {
                    markerIDs.Add(id,
                        new MarkerData {touchingLastFrame = false, pos = _pos, pensize = _pensize, colorRGB = _color});
                }
                else
                {
                    markerIDs[id].pos = _pos;
                }

                markerIDs[id].color = SetColor(new Color(_color[0], _color[1], _color[2]), id);

                //int x = (int)(markerIDs[id].pos[0] * TextureSize - markerIDs[id].pensize/2);
                //int x = (int) (markerIDs[id].pos[0] * texture.width - markerIDs[id].pensize / 2);
                int x = (int) (markerIDs[id].pos[0] * texture.width - markerIDs[id].pensize - 1);
                //int y = (int)(markerIDs[id].pos[1] * TextureSize - markerIDs[id].pensize / 2);
                //int y = (int) (markerIDs[id].pos[1] * texture.height - markerIDs[id].pensize / 2);
                int y = (int) (markerIDs[id].pos[1] * texture.height - markerIDs[id].pensize - 1);

                if (x < 0)
                {
                    //x = 0;
                    x = markerIDs[id].pensize + 1;
                }

                if (x >= texture.width - markerIDs[id].pensize - 1)
                {
                    //x = texture.width;
                    x = texture.width - -markerIDs[id].pensize - 1;
                }

                if (y < 0)
                {
                    //y = 0;
                    y = markerIDs[id].pensize + 1;
                }

                if (y >= texture.height - markerIDs[id].pensize - 1)
                {
                    //y = texture.height;
                    y = texture.height - markerIDs[id].pensize - 1;
                }


                //If last frame was not touching a marker, we don't need to lerp from last pixel coordinate to new, so we set the last coordinates to the new.
                if (!markerIDs[id].touchingLastFrame)
                {
                    markerIDs[id].lastX = (float) x;
                    markerIDs[id].lastY = (float) y;
                    markerIDs[id].touchingLastFrame = true;
                }

                if (markerIDs[id].touchingLastFrame)
                {
                    texture.SetPixels(x, y, markerIDs[id].pensize, markerIDs[id].pensize, markerIDs[id].color);

                    //Lerp last pixel to new pixel, so we draw a continuous line.
                    for (float t = 0.01f; t < 1.00f; t += 0.1f)
                    {
                        int lerpX = (int) Mathf.Lerp(markerIDs[id].lastX, (float) x, t);
                        int lerpY = (int) Mathf.Lerp(markerIDs[id].lastY, (float) y, t);

                        /*
                        if (lerpX < 0)
                        {
                            //lerpX = 0;
                            lerpX = markerIDs[id].pensize + 1;
                        }
    
                        if (lerpX >= texture.width)
                        {
                            //lerpX = texture.width - 1;
                            lerpX = texture.width - markerIDs[id].pensize - 1;
                        }
                        
                        if (lerpY < 0)
                        {
                            lerpY = 0;
                            lerpY = markerIDs[id].pensize + 1;
                        }
    
                        if (lerpY >= texture.height)
                        {
                            //lerpY = texture.height - 1;
                            lerpY = texture.height - markerIDs[id].pensize - 1;
                        }
                        */

                        if (lerpX <= (markerIDs[id].pensize + 1))
                        {
                            //lerpX = 0;
                            lerpX = markerIDs[id].pensize + 1;
                        }

                        if (lerpX >= (texture.width - markerIDs[id].pensize - 1))
                        {
                            //lerpX = texture.width - 1;
                            lerpX = texture.width - markerIDs[id].pensize - 1;
                        }

                        if (lerpY < (markerIDs[id].pensize + 1))
                        {
                            lerpY = 0;
                            lerpY = markerIDs[id].pensize + 1;
                        }

                        if (lerpY >= texture.height - markerIDs[id].pensize - 1)
                        {
                            //lerpY = texture.height - 1;
                            lerpY = texture.height - markerIDs[id].pensize - 1;
                        }

                        Debug.Log(lerpX + " " + lerpY);
                        texture.SetPixels(lerpX, lerpY, markerIDs[id].pensize, markerIDs[id].pensize,
                            markerIDs[id].color);
                    }

                    //so it runs once per frame even if multiple markers are touching the whiteboard
                    if (!applyingTexture)
                    {
                        applyingTexture = true;
                        ApplyTexture();
                    }
                }

                markerIDs[id].lastX = (float) x;
                markerIDs[id].lastY = (float) y;
            }
            catch
            {
                Debug.Log("Error DrawAtPosition");
            }
            finally
            {
            }
        }

        public void ApplyTexture()
        {
            texture.Apply();
            applyingTexture = false;
        }

        [PunRPC]
        public void ResetTouch(int id)
        {
            if (markerIDs.ContainsKey(id))
                markerIDs[id].touchingLastFrame = false;
        }

        //Creates the color array for the marker id
        public Color[] SetColor(Color color, int id)
        {
            return Enumerable.Repeat(color, markerIDs[id].pensize * markerIDs[id].pensize).ToArray();
        }

        //To clear the whiteboard.
        public void ClearWhiteboard()
        {
            pv.RPC("RPC_ClearWhiteboard", RpcTarget.AllBuffered);
        }

        [PunRPC]
        public void RPC_ClearWhiteboard()
        {
            texture.SetPixels(whitePixels);
            texture.Apply();
        }
    }
}